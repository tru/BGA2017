#!/bin/sh
# https://gitlab.pasteur.fr/tru/BGA2017/raw/master/okeanos-global/create-vdb-c7.sh
yum -y install wget

parted -s /dev/vdb mklabel msdos
parted -s /dev/vdb mkpart primary ext2 1m 100
parted -s /dev/vdb set 1 boot on
mkfs.ext2 -m 0  -L XXX /dev/vdb1
mkdir /a
mount /dev/vdb1 /a
mkdir -p /a/extlinux
wget -O  /a/extlinux/extlinux.conf https://gitlab.pasteur.fr/tru/BGA2017/raw/master/okeanos-global/extlinux.conf

mkdir -p /a/extlinux/centos/7.3.1611/x86_64
for k in initrd.img vmlinuz
do
wget -O /a/extlinux/centos/7.3.1611/x86_64/$k http://ftp.ntua.gr/pub/linux/centos/7.3.1611/os/x86_64/images/pxeboot/$k
done

if [ ! -f syslinux-6.03/bios/extlinux/extlinux ]; then
yum -y install glibc.i686
wget -c https://www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-6.03.tar.gz
tar xzvf syslinux-6.03.tar.gz
fi
syslinux-6.03/bios/extlinux/extlinux --install /a/extlinux
dd if=syslinux-6.03/bios/mbr/mbr.bin of=/dev/vdb

(cd /a && find . -ls)
umount /a

