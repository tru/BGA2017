#!/bin/sh
# https://gitlab.pasteur.fr/tru/BGA2017/raw/master/okeanos-global/create-vdb-ipxe.usb.sh
yum -y install wget

wget -O ipxe.usb https://gitlab.pasteur.fr/tru/BGA2017/raw/master/okeanos-global/ipxe.usb
dd if=ipxe.usb of=/dev/vdb

