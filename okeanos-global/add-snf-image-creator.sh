#!/bin/sh
# https://github.com/grnet/synnefo/issues/394
# https://gitlab.pasteur.fr/tru/BGA2017/raw/master/okeanos-global/add-snf-image-creator.sh
# 
cd /etc/yum.repos.d &&  curl -O http://download.opensuse.org/repositories/home:/GRNET:/synnefo/CentOS_7/home:GRNET:synnefo.repo
yum -y install snf-image-creator python-argparse
