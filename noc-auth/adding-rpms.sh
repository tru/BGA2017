#!/bin/sh
echo adding more packages 
yum -y install epel-release
yum -y update
yum -y install vim-enhanced wget screen tmux
yum -y groupinstall -y development
yum -y install java-1.8.0-openjdk.x86_64
yum -y install environment-modules
# minimal desktop for x2go
yum -y groupinstall mate-desktop-environment 
yum -y install x2goserver.x86_64

yum -y install zlib-devel # bwa
yum -y install ncurses-devel bzip2-devel xz-devel curl-devel openssl-devel # samtools
yum -y install bash-completion
# fredj
yum -y install emacs
# basic editor:
yum -y install gedit nedit

#circos requires gd-devel to build perl-GD
yum -y install gd-devel
yum -y install libreoffice
yum -y install evince xpdf 
yum -y install eog elinks
yum -y install cmake parallel # freebayes
yum -y install hdf5-static.x86_64 hdf5-devel.x86_64
yum -y install glibc-static libstdc++-static # muscle build for micca

# R
yum -y install atlas-devel blas-devel lapack-devel # R
yum -y install readline-devel libXt-devel.x86_64 # R
yum -y install libtiff-devel cairo-devel libicu-devel # R
yum -y install libxml2-devel # R/tidyverse

# gvim
yum -y install vim-X11

# pdsh
yum -y install pdsh-rcmd-ssh.x86_64

# fex:
yum -y perl-Digest-MD5 bind-utils perl-digest

yum -y install nano zsh

