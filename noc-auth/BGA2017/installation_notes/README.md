Various notes on installing the software for the workshop.

```
/BGA2017/
├── anaconda2
├── anaconda3
├── bedtools2-2.26.0
├── blast-2.2.26-x64-linux
├── build
│   ├── freebayes
│   ├── micca-1.6.2
│   ├── perl-5.24.1
│   ├── picrust-1.1.1
│   ├── python2-micca
│   ├── R-3.4.0
│   ├── samtools-1.4.1
│   ├── vcflib
│   └── vcftools-0.1.14
├── bwa-0.7.15
├── Course_material <-- course material arranged per day
│   ├── June05
│   ├── June06
│   ├── June07
│   ├── June08
│   ├── June09
│   ├── June10
│   ├── June11
│   ├── June12
│   ├── June13
│   ├── June14
│   ├── June15
│   ├── June16
│   └── June17
├── fex
├── freebayes
├── GenomeAnalysisTK-3.7
├── IGV_2.3.93
├── MEGAN
├── modulefiles  <--- configuration files specific to the course
│   ├── bedtools
│   ├── bwa
│   ├── fex
│   ├── freebayes
│   ├── GATK -> GenomeAnalysisTK
│   ├── GenomeAnalysisTK
│   ├── IGV
│   ├── MEGAN
│   ├── ncbi-blast
│   ├── ncbi-blast+
│   ├── Oracle_java
│   ├── perl
│   ├── picard
│   ├── R
│   ├── rdp_classifier
│   ├── samtools
│   ├── sratoolkit
│   ├── sublime_text_3
│   └── vcftools
├── ncbi-blast-2.6.0+-x64-linux
├── Oracle_java
├── perl-5.24.1
├── picard-2.9.2
├── R-3.4.0
├── rdp_classifier_2.11
├── samtools-1.4.1
├── sources <-- archives of the installed programs whenever possible (cpan/R put things in your $HOME)
│   ├── claudio
│   ├── daniel
│   ├── gbenson
│   ├── martink
│   ├── misc
│   ├── stephan
│   └── tekaia
├── sratoolkit.2.8.2-1-centos_linux64
├── sublime_text_3_build_3126_x64
└── vcftools-0.1.14

```

