mkdir -p /home/centos/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1k/QM9bIIGYzgRSuUD3dHFH7UtvNBggiqoL2B4+3paUi4bFffbf6a6wfpZGZNC9tFzWZj1/DxZ36WZgmpWq4R9XV6iyUayOB8qaFQw08GHV+wmSO1Z8ZZgs5snqWQ/AXQNRVhFEV+VKBB6/sCBz3haIN51epwo5r5NoTaKAZ33gH1mjUDRj5CvUwWSWpEg1QeO3woC9jm8eBxRHQMHRBr+SSLSh/V4DCimMkdJpeOYrtzHsEQp3QJFDc0IGU/XXDL6zU0GooI9HcXAd5LUkUhtxRdLgyGFRcmI4n8idCyktBmVkB7myF1j6MUycjHCOz5y3UsJq87AcvysvKXmZCj noc-auth' > /home/centos/.ssh/authorized_keys
chmod 0700 /home/centos/.ssh
chmod 0600 /home/centos/.ssh/authorized_keys
chown -R centos:centos /home/centos/.ssh
restorecon -rv /home/centos
echo 'centos ALL=(ALL) ALL' >> /etc/sudoers
restorecon -rv /etc/sudoers
