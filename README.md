# Mandatory links
- http://webext.pasteur.fr/tekaia/BCGA2017.html
- course program: http://webext.pasteur.fr/tekaia/BCGA2017/BCGA2017_Prog.html

- framapad course folder: https://goo.gl/GXqrf7 or https://mypads.framapad.org/mypads/index.html?/mypads/group/bca2017-jp4v497d9/view (password protected)
